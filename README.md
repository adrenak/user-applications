# README #

### What is this repository for? ###

* This repository offers an interface for interacting with the applications installed on the user's device.

* Version 1.0

### How do I get set up? ###

* Requires Unity5.2.2+ Android

* Deployment instructions:
+ `public static bool UserApps.IsApplicationInstalled(string packageName);` returns if an applications with the package name `packageName` is installed on the device or not.
+ `public static bool UserApps.LaunchApplication(string packageName);` launches the application with package name `packageName` provided it is installed. So, always check if the app is installed or not before attempting to launch it.

### Possible Improvements ###
* Add more features to the class.
* Fix the `IsApplicationRunning` method

### Who do I talk to? ###

* Vatsal Ambastha
+ vatsal (at) FirexitSoftware (dot) com