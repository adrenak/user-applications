﻿using UnityEngine;

public class UserApps{
	
	static AndroidJavaClass unityPlayer = null;
	static AndroidJavaObject currentActivity = null;
	static AndroidJavaObject packageManager = null;
	static AndroidJavaObject installedPackages = null;
	static AndroidJavaObject installedApplications = null;

	static string packageNameNotInstalledMsg = "NOT_INSTALLED";

	// ================================================
	// Init routines begin
	/// <summary>
	/// Initializes the classes' native android objects.
	/// </summary>
	public static void Init() {
		AssertUnityPlayer();
		AssertCurrentActivity();
		AssertPackageManager();
		LoadInstalledPackages();
		LoadInstalledApplications();
	}

	static void AssertUnityPlayer(){
		if(unityPlayer == null)
			unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
	}
	static void AssertCurrentActivity(){
		if(currentActivity == null)
			currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
	}
	static void AssertPackageManager(){
		if(packageManager == null)
			packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager");
	}	
	static void LoadInstalledPackages(){
		installedPackages = packageManager.Call<AndroidJavaObject>("getInstalledPackages",0);
	}
	static void LoadInstalledApplications(){
		installedApplications = packageManager.Call<AndroidJavaObject>("getInstalledApplications",0);
	}
	// Init routines end
	// ================================================

	// ================================================
	// EXPOSED FEATURE METHODS BEGIN
	/// <summary>
	/// Check if a particular application is installed on the user's device using the bundle identifier.
	/// The bool flag is set to true if the application list should be reloaded (heavy, slight jitter on devices).
	/// </summary>
	/// <param name="_applicationPackageName"></param>
	/// <param name="_doReloadList"></param>
	public static bool IsApplicationInstalled(string _applicationPackageName, bool _doReloadList = false) {
		HandleListReload(_doReloadList);
		string flags = GetApplicationFlags(_applicationPackageName);

		if(flags != packageNameNotInstalledMsg)
			return true;
		return false;
	}

	/// <summary>
	/// Returns true if the application has ever been launched, and not shut down manually through the task manager.
	/// *** TODO: FLAGS ARE PROBABLY INCORECT. THE FUNCTION RETURNS FALSE ALL THE TIME RIGHT NOW. NEEDS WORK, DON'T USE. ***
	/// </summary>
	/// <param name="_applicationPackageName"></param>
	/// <param name="_doReloadList"></param>
	/// <returns></returns>
	[System.Obsolete]
	public static bool IsApplicationRunning(string _applicationPackageName, bool _doReloadList = false) {
		HandleListReload(_doReloadList);
		string flags = GetApplicationFlags(_applicationPackageName);

		if(flags != packageNameNotInstalledMsg) {
			if(flags.Length >= 5) {
				if(flags[flags.Length-1-5] == '2')
					return true;
			}
		}
		return false;
	}

	/// <summary>
	/// Launches an application if it is currently installed.
	/// </summary>
	/// <param name="_applicationPackageName"></param>
	public static void LaunchApplication(string _applicationPackageName) {
		if(IsApplicationInstalled(_applicationPackageName))
			currentActivity.Call("startActivity", GetLaunchIntentFromPackageName(_applicationPackageName));
	}
	// EXPOSED FEATURE METHODS END
	// ================================================

	static void HandleListReload(bool _doReloadList) {
		if(_doReloadList) {
			LoadInstalledPackages();
			LoadInstalledApplications();
		}
	}

	static string GetApplicationFlags(string _applicationPackageName) {
		for(int i = 0; i < GetInstalledPackagesSize(); i++) {
			AndroidJavaObject installedPackage = GetInstalledPackageByIndex(i);
			AndroidJavaObject installedApplication = GetInstalledApplicationByIndex(i);
			string packageNew = GetInstalledPackageName(installedPackage);

			if(packageNew.Equals(_applicationPackageName)) {
				return installedApplication.Get<int>("flags").ToString("X");
			}
		}
		return packageNameNotInstalledMsg;
	}

	static int GetInstalledPackagesSize() {
		return installedPackages.Call<int>("size");
	}
	static AndroidJavaObject GetInstalledPackageByIndex(int _index) {
		return installedPackages.Call<AndroidJavaObject>("get", _index);
	}
	static AndroidJavaObject GetInstalledApplicationByIndex(int _index) {
		return installedApplications.Call<AndroidJavaObject>("get", _index);
	}
	static string GetInstalledPackageName(AndroidJavaObject _installedPackage) {
		return _installedPackage.Get<string>("packageName");
	}
	static AndroidJavaObject GetLaunchIntentFromPackageName(string _applicationPackageName) {
		return packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", _applicationPackageName);
	}
}