﻿/// <summary>
/// Checks if the Facebook android application is installed on the device.
/// If it is, then the context script launches it.
/// </summary>

using UnityEngine;

public class UserAppsContext : MonoBehaviour {

	void Start() {
		UserApps.Init();
		if(UserApps.IsApplicationInstalled("com.facebook.katana"))
			UserApps.LaunchApplication("com.facebook.katana");
	}
}
